const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const path = require('path');

var knex = require('knex')({
    client: 'pg',
    connection: {
      host : '127.0.0.1',
      user : 'roman',
      port: 5432,
      password : 'roman',
      database : 'test',
     
    },
    acquireConnectionTimeout: 10000, 
    // debug: true,
    pool: {
        min: 0, 
        max: 10
      }
  });

const bodyParser = require('body-parser');
let env = process.env.NODE_ENV || 'development';

let chatUsersOnline = [];

const allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}

app.use(allowCrossDomain);
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/', function(req, res){
    res.sendFile('/index.html');
  });

app.get('/messages', (req, res) => {
    return knex.raw(`SELECT * FROM 
                        (SELECT * FROM messages ORDER BY message_id DESC LIMIT 100) 
                    sub ORDER BY message_id ASC`)
    .then((result) => {
        return res.status(200).send(JSON.stringify(result.rows));
    })
    .catch(err => {
        console.log("err", err);
        return res.status(500);
    } )
});

io.on('connect', (socket) => {
 
    socket.on('message', function (data) {
        data = JSON.parse(data);

        if (data && data.author) {
            return knex('messages').insert({
                author: data.author,
                content: data.content,
                created_at: data.created_at
            })
            .then(io.emit('messages', [data]))
            .catch(err => console.log("connect message", err));       
        } else return null;
    });

    socket.on('user', function (data) {
        return knex.raw(`SELECT DISTINCT author FROM 
                            (SELECT * FROM messages ORDER BY message_id DESC LIMIT 10) 
                        sub ORDER BY author ASC`)
            .then((result) => {
                result = result.rows.map(author => author.author);
                chatUsersOnline = [...result];
                if (result.includes(data.login)) return io.emit('users', []);
                else {
                    result.push(data.login);
                    return io.emit('users', result);
                }
            })
  
    });
    return socket.on('disconnect', function () {
        return io.emit('users', chatUsersOnline);
    });
});

http.listen(8080, function () {
    if (env !== 'test') {
        return console.log('listening on *:8080');
    }
});
