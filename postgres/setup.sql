CREATE TABLE messages (
    message_id serial PRIMARY KEY,
    author VARCHAR (100) NOT NULL DEFAULT 'John Doe',
    created_at TIMESTAMPTZ,
    content TEXT 
);


INSERT INTO messages (author, created_at, content)
VALUES ('Shannon','1980-01-01 22:01:14','Freeman lives'),
      ('Sheila','1981-02-05','Wells Fargo');