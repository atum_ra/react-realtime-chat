# React realtime chat, based on express.js. knex.js, socket.io

## Features:

* real time broadcast for all connected clients
* persistance with Postgresql
* view with pool of connected users
* restriction for chat connect without name or with the same name
* optimization with in-memory cache of read/write Postgres operations for high-load
  
## TODO

* session control with 'express-socket.io-session' (socket.handshake.session)